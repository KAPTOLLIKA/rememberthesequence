package Game;

import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.Random;

public class Game extends JFrame {

    private final int sizeX = 820;
    private final int sizeY = 720;

    int lastI = 0;

    String[] sounds = new String[9];

    int buttonsCount = 4;
    int oneLineButtonsCount = (int) Math.sqrt((double) buttonsCount);
    final int betweenButtonsSpace = 10;

    int[] lastTurn = {new Random().nextInt(buttonsCount)};

    JButton[] buttons = new JButton[buttonsCount];

    int buttonSize = (sizeY - 120 - betweenButtonsSpace) / oneLineButtonsCount;

    int SCORE = 0;

    boolean playersTurn = false;
    boolean gameOver = false;

    public Game() {
        for (int i = 0; i < sounds.length; ++i) {
            sounds[i] = "Sounds\\" + i + ".wav";
        }

        int locationX = ((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth() - sizeX) / 2;
        int locationY = ((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight() - sizeY) / 2;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(sizeX, sizeY);
        setLocation(locationX, locationY);
        setResizable(false);
    }

    public void start() {
        boolean firstEnter = true;
        GamePanel panel = new GamePanel(this);
        getContentPane().add(panel);
        setVisible(true);
        passTime(1500);
        while (!gameOver) {
            if (!playersTurn) {
                if (!firstEnter)
                    lastTurn = panel.getNextTurn(this, lastTurn);
                panel.showTurn(this, lastTurn);
                playersTurn = true;
            } else {
                if (firstEnter)
                    firstEnter = false;
                if (lastI == lastTurn.length) {
                    playersTurn = false;
                    lastI = 0;
                    ++SCORE;
                    passTime(1000);
                }
            }
            panel.score.setText("Score: " + SCORE);
        }
        passTime(1000);
        dispose();
    }

    Clip getClip(String Sound) {
        try {
            File soundFile = new File(Sound);
            Clip clip;
            AudioInputStream stream = AudioSystem.getAudioInputStream(soundFile);
            AudioFormat format = stream.getFormat();
            DataLine.Info info = new DataLine.Info(Clip.class, format);
            Clip audioClip = (Clip) AudioSystem.getLine(info);
            audioClip.open(stream);
            clip = audioClip;
            return clip;
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e.getMessage());
            System.exit(0);
        }
        return getClip("1#|!@4%!#*(U");
    }

    void passTime(int millis) {
        try {
            Thread.sleep(millis);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Some error occured :(");
        }
    }

    Color getColor(int i) {
        if (i == 0)
            return new Color(155, 155, 0);
        else if (i == 1)
            return new Color(0, 0, 155);
        else if (i == 2)
            return new Color(155, 0, 0);
        else if (i == 3)
            return new Color(0, 155, 0);
        return Color.BLACK;
    }
}
