package Game;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class GameButtonListener implements ActionListener {

    private Game game;

    private int i;

    GameButtonListener(Game game, int i) {
        this.game = game;
        this.i = i;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (game.playersTurn) {
            if (i == game.lastTurn[game.lastI]) {
                game.getClip(game.sounds[i]).start();
                game.buttons[i].setBackground(game.buttons[i].getBackground().brighter());
                game.passTime(100);
                game.buttons[i].setBackground(game.getColor(i));
                ++game.lastI;
            } else {
                game.gameOver = true;
                game.getClip("Sounds\\fail.wav").start();
                JOptionPane.showMessageDialog(null, "Your score is " + game.SCORE);
            }
        }
    }
}
