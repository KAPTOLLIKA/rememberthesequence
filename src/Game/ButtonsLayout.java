package Game;

import java.awt.*;

public class ButtonsLayout implements LayoutManager {

    private Game game;

    ButtonsLayout(Game game) {
        this.game = game;
    }

    @Override
    public void addLayoutComponent(String name, Component comp) {
    }

    @Override
    public void removeLayoutComponent(Component comp) {
    }

    @Override
    public Dimension preferredLayoutSize(Container parent) {
        return null;
    }

    @Override
    public Dimension minimumLayoutSize(Container parent) {
        return null;
    }

    @Override
    public void layoutContainer(Container container) {
        Component list[] = container.getComponents();
        list[0].setBounds(20, 0, 150, 50);
        int x = (game.getWidth() - game.oneLineButtonsCount * game.buttonSize) / 2;
        int y = list[0].getHeight() + 15;
        for (int i = 1; i < list.length; ++i) {
            list[i].setBounds(x, y, game.buttonSize, game.buttonSize);
            x += game.buttonSize + game.betweenButtonsSpace;
            if (i % game.oneLineButtonsCount == 0) {
                x = (game.getWidth() - game.oneLineButtonsCount * game.buttonSize) / 2;
                y += game.buttonSize + game.betweenButtonsSpace;
            }
        }
    }
}
