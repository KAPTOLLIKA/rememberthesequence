package Game;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

class GamePanel extends JPanel {

    JLabel score = new JLabel();

    GamePanel(Game game) {
        setLayout(new ButtonsLayout(game));

        setBackground(Color.BLACK);

        score.setText("Score: " + game.SCORE);

        score.setFont(new Font("SansSerif", Font.BOLD, 20));

        score.setForeground(Color.WHITE);

        add(score);

        for (int i = 0; i < game.buttonsCount; ++i) {
            JButton button = new JButton();
            button.setBackground(game.getColor(i));
            button.addActionListener(new GameButtonListener(game, i));
            game.buttons[i] = button;
            add(game.buttons[i]);
        }
    }

    int[] getNextTurn(Game game, int[] lastTurn) {
        int[] result = new int[lastTurn.length + 1];

        System.arraycopy(lastTurn, 0, result, 0, lastTurn.length);

        result[lastTurn.length] = new Random().nextInt(game.buttonsCount);

        return result;
    }

    void showTurn(Game game, int[] turn) {
        for (int i : turn) {
            game.buttons[i].setBackground(game.buttons[i].getBackground().brighter());
            game.getClip(game.sounds[i]).start();
            game.passTime(500);
            game.buttons[i].setBackground(game.getColor(i));
        }
    }
}
