import Game.Game;

public class Main {
    public static void main(String[] args) {
        Game game;
        while (true) {
            game = new Game();
            game.start();
        }
    }
}
